--TABELLA ARMADIETTO
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (1,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (2,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (3,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (4,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (5,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (6,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (7,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (8,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (9,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (10,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (11,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (12,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (13,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (14,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (15,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (16,2);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (17,3);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (18,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (19,1);
INSERT INTO ARMADIETTO (Numero,Dimensione) VALUES (20,3);



--TABELLA STALLA
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (1,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (2,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (3,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (4,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (5,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (6,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (7,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (8,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (9,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (10,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (11,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (12,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (13,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (14,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (15,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (16,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (17,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (18,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (19,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (20,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (21,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (22,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (23,20);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (24,25);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (25,15);
INSERT INTO STALLA (Numero_stalla,Dimensione) VALUES (26,20);


--Tabella PROPRIETARIO
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000001','Luigi','Visciano','Via Petrarca','80100','Napoli','333112231');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000002','Franco','Chiaro','Via Grosso','80123','Volla','3331122341');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000003','Maria','Capuano','Via Chiatamone','80100','Napoli','333144331');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000004','Salvatore','Esposito','Via Roma','80100','Napoli','3332211321');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000005','Giovanna','Miraggio','Via Petrarca','80100','Napoli','323116221');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000006','Carmela','Mastro','Via Panico','80100','Napoli','3231114312');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000007','Mario','Pinto','Via Giacomo Leopardi','80123','Volla','3321233421');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000008','Giacomo','Cuomo','Via Pntedera','80100','Napoli','333010231');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000009','Franco','Calmo','Via Casale','80123','Volla','3321412341');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000010','Maria','Chinati','Via Grosso','80100','Napoli','343104331');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000011','Salvatore','Scogliamiglio','Via Savini','80100','Napoli','3232261521');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000012','Giovanna','Cantone','Via Gianturco','80100','Napoli','323010231');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000013','Cristina','Miracolo','Via Priscopo','80100','Napoli','33322331234');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000014','Mario','Brinta','Via Giacomo Leopardi','80123','Volla','33321638299');
INSERT INTO PROPRIETARIO (Codice_fisc,Nome,Cognome,Via,Cap,Citta,Telefono)VALUES('AAA0000000000015','Mario','Basito','Via Santificato','8003','Portici','3241122312');




--Tabella STALLIERE
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(101,'Diego','Castaldo',1300,1,'333011311');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(102,'Frank','Bianco',1200,2,'333011233');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(103,'Carlo','Conte',1000,3,'322123453');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(104,'Giorgia','Palmas',900,4,'366001311');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(105,'Mario','Rossi',1150,5,'362022313');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(106,'Nicola','Giordano',1000,6,'356001315');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(107,'Davide','Chiaro',1300,7,'3922219488');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(108,'Frank','Carpentieri',1200,8,'3662211840');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(109,'Francesca','Finocchiaro',1000,9,'3332215164');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(110,'Giorgia','Camaleonte',980,10,'3921100240');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(111,'Mario','Giustino',1150,11,'3332211567');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(112,'Nicola','Composto',1000,12,'332123422');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(113,'Jessica','Carotenuto',1400,13,'3331230012');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(114,'Mario','Mastino',1150,14,'3284411532');
INSERT INTO STALLIERE (Numero_tesserino,Nome,Cognome,Salario,Num_arm,Telefono) VALUES(115,'Nicola','Borghetto',1000,15,'3345166120');





--Tabella CAVALLO
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY01','Rocky','Brumby',700,'M',101,1,'AAA0000000000001');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY02','Jack','Noriker',750,'M',102,2,'AAA0000000000002');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY03','Principessa','Noriker',650,'F',103,3,'AAA0000000000003');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY04','Tyson','Brumby',850,'M',104,4,'AAA0000000000004');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY05','Carmen','Mangalarga',580,'F',105,5,'AAA0000000000005');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY06','Marlen','Mangalarga',600,'F',106,6,'AAA0000000000006');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY07','John','Noriker',750,'M',107,7,'AAA0000000000007');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY08','Bobby','Brumby',790,'M',108,8,'AAA0000000000008');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY09','James','Noriker',750,'M',109,9,'AAA0000000000009');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY10','Tina','Mangalarga',750,'F',110,10,'AAA0000000000010');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY11','Jessica','Mangalarga',580,'F',111,11,'AAA0000000000011');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY12','Maria','Mangalarga',600,'F',112,12,'AAA0000000000012');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY13','Batman','Noriker',750,'M',113,13,'AAA0000000000013');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY14','Pippo','Brumby',740,'M',114,14,'AAA0000000000014');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY15','Sonic','Noriker',750,'M',115,15,'AAA0000000000015');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY16','Mignolina','Mangalarga',750,'F',111,16,'AAA0000000000014');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY17','Margot','Mangalarga',750,'F',111,17,'AAA0000000000001');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY18','Peater','Noriker',850,'M',112,18,'AAA0000000000014');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY19','Fulmine','Brumby',900,'M',101,19,'AAA0000000000012');
INSERT INTO CAVALLO (Microchip,Nome,Razza,Peso,Sesso,Tesserino,Numero_stalla,CF) VALUES ('AXY20','Faster','Mangalarga',750,'M',102,20,'AAA0000000000011');


--Tabella Entrata
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(1, to_date('09-01-2014', 'dd-mm-yyyy'), 'AXY01');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(2, to_date('10-03-2014', 'dd-mm-yyyy'), 'AXY02');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(3, to_date('03-01-2014', 'dd-mm-yyyy'), 'AXY03');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(4, to_date('09-03-2014', 'dd-mm-yyyy'), 'AXY04'); 
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(5, to_date('08-02-2014', 'dd-mm-yyyy'), 'AXY05');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(6, to_date('09-02-2014', 'dd-mm-yyyy'), 'AXY06');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(7, to_date('19-01-2014', 'dd-mm-yyyy'), 'AXY07');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(8, to_date('08-01-2014', 'dd-mm-yyyy'), 'AXY08');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(9, to_date('09-02-2014', 'dd-mm-yyyy'), 'AXY09');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(10, to_date('09-03-2014', 'dd-mm-yyyy'), 'AXY10');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(11, to_date('02-04-2014', 'dd-mm-yyyy'), 'AXY11');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(12, to_date('01-03-2014', 'dd-mm-yyyy'), 'AXY12');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(13, to_date('01-03-2014', 'dd-mm-yyyy'), 'AXY12');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(14, to_date('24-05-2014', 'dd-mm-yyyy'), 'AXY14');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(15, to_date('14-06-2014', 'dd-mm-yyyy'), 'AXY15');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(16, to_date('14-05-2014', 'dd-mm-yyyy'), 'AXY16');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(17, to_date('12-05-2014', 'dd-mm-yyyy'), 'AXY17');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(18, to_date('11-06-2014', 'dd-mm-yyyy'), 'AXY18');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(19, to_date('27-09-2014', 'dd-mm-yyyy'), 'AXY19');
INSERT INTO ENTRATA (IDEntrata,Data,Microchip) VALUES(20, to_date('17-07-2014', 'dd-mm-yyyy'), 'AXY20');


--Tabella FOTO
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Rocky.JPG', to_date('09-01-2014', 'dd-mm-yyyy'), 'AXY01');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Jack.JPG', to_date('10-03-2014', 'dd-mm-yyyy'), 'AXY02');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Principessa.JPG', to_date('03-01-2014', 'dd-mm-yyyy'), 'AXY03');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Tyson.JPG', to_date('09-03-2014', 'dd-mm-yyyy'), 'AXY04');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Carmen.JPG', to_date('08-02-2014', 'dd-mm-yyyy'), 'AXY05');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Marlen.JPG', to_date('09-02-2014', 'dd-mm-yyyy'), 'AXY06');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('John.JPG', to_date('19-01-2014', 'dd-mm-yyyy'), 'AXY07');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Bobby.JPG', to_date('08-01-2014', 'dd-mm-yyyy'), 'AXY08');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('James.JPG', to_date('09-02-2014', 'dd-mm-yyyy'), 'AXY09');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Tina.JPG', to_date('09-03-2014', 'dd-mm-yyyy'), 'AXY10');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Jessica.JPG', to_date('02-04-2014', 'dd-mm-yyyy'), 'AXY11');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Maria.JPG', to_date('01-03-2014', 'dd-mm-yyyy'), 'AXY12');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Batman.JPG', to_date('11-01-2014', 'dd-mm-yyyy'), 'AXY13');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Pippo.JPG', to_date('24-05-2014', 'dd-mm-yyyy'), 'AXY14');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Sonic.JPG', to_date('14-06-2014', 'dd-mm-yyyy'), 'AXY15');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Mignolina.JPG', to_date('14-05-2014', 'dd-mm-yyyy'), 'AXY16');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Margot.JPG', to_date('12-05-2014', 'dd-mm-yyyy'), 'AXY17');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Peater.JPG', to_date('11-06-2014', 'dd-mm-yyyy'), 'AXY18');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Fulmine.JPG', to_date('27-09-2014', 'dd-mm-yyyy'), 'AXY19');
INSERT INTO FOTO (Url,Data,Microchip) VALUES ('Faster.JPG', to_date('17-07-2014', 'dd-mm-yyyy'), 'AXY20');


--Tabella VISITAVETERINARIA
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(01,to_date('08-07-2014', 'dd-mm-yyyy'),'Nessun sintomo, falso allarme', null,'AXY01');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(02,to_date('05-04-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY04');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(03,to_date('08-04-2014', 'dd-mm-yyyy'),'Problema ad una zampa', 'Una settimana di riposo','AXY01');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(04,to_date('18-06-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY05');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(05,to_date('10-05-2014', 'dd-mm-yyyy'),'Problemi intestinali ', 'Una settimana di antibiotico','AXY06');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(06,to_date('11-04-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY07');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(07,to_date('19-06-2014', 'dd-mm-yyyy'),'Problema ad una zampa', 'Una settimana di riposo','AXY09');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(08,to_date('28-07-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY02');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(09,to_date('18-07-2014', 'dd-mm-yyyy'),'Problemi intestinali', 'Una settimana di antibiotico','AXY03');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(10,to_date('22-02-2014', 'dd-mm-yyyy'),'Problema ad una zampa', 'Una settimana di riposo','AXY04');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(11,to_date('18-03-2014', 'dd-mm-yyyy'),'Nessun sintomo, falso allarme', null,'AXY04');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(12,to_date('15-02-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY14');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(13,to_date('18-04-2014', 'dd-mm-yyyy'),'Problema ad una zampa', 'Una settimana di riposo','AXY11');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(14,to_date('11-06-2014', 'dd-mm-yyyy'),'Febbre', 'Una settimana di antibiotico','AXY15');
INSERT INTO VISITAVETERINARIA (IDVisita,Data,Referto,Terapia,Microchip)VALUES(15,to_date('10-05-2014', 'dd-mm-yyyy'),'Problemi intestinali ', 'Una settimana di antibiotico','AXY16');

commit;


