------------------------
-- ripulitura db
------------------------
--
DROP TABLE ENTRATA;
DROP TABLE VISITAVETERINARIA;
DROP TABLE FOTO;
DROP TABLE CAVALLO;
DROP TABLE STALLIERE;
DROP TABLE PROPRIETARIO;
DROP TABLE STALLA;
DROP TABLE ARMADIETTO;

-------------------------
-- creazione tabelle db
-------------------------

CREATE TABLE ARMADIETTO(
	Numero Number PRIMARY KEY,
	Dimensione Number NOT NULL 
);




CREATE TABLE STALLA(
	Numero_stalla Number PRIMARY KEY,
	Dimensione Number
);

CREATE TABLE PROPRIETARIO(
	Codice_fisc char (16) PRIMARY KEY,
	Nome VARCHAR (20),
	Cognome VARCHAR (20),
	Via VARCHAR (50),
	Cap VARCHAR (10),
	Citta VARCHAR (50),
	Telefono VARCHAR(15) UNIQUE
);


CREATE TABLE STALLIERE(
	Numero_tesserino Number PRIMARY KEY,
	Nome VARCHAR (20),
	Cognome VARCHAR(20),
	Salario Number,
	Num_arm Number NOT NULL,
	Telefono VARCHAR(15)  UNIQUE,
	CONSTRAINT FK_Armad FOREIGN KEY (Num_arm) REFERENCES ARMADIETTO(Numero) ON DELETE SET NULL
);



CREATE TABLE CAVALLO(
	Microchip char(5) PRIMARY KEY,
	Nome VARCHAR(20),
	Razza VARCHAR (20),
	Peso Number,
	Sesso char(1) NOT NULL CHECK(Sesso IN('M','F')),
	Tesserino Number  NOT NULL,
	Numero_stalla Number  NOT NULL,
	CF char(16),
	CONSTRAINT FK_Proprietario FOREIGN KEY (CF) REFERENCES PROPRIETARIO(Codice_fisc)  ON DELETE CASCADE ,
	CONSTRAINT FK_Stalliere FOREIGN KEY (Tesserino) REFERENCES STALLIERE(Numero_tesserino) ON DELETE CASCADE ,
	CONSTRAINT FK_Stalla FOREIGN KEY (Numero_stalla) REFERENCES STALLA (Numero_stalla)  ON DELETE CASCADE 
);


CREATE TABLE FOTO(
	Url VARCHAR(50) PRIMARY KEY,
	Data DATE,
	Microchip char(5)  NOT NULL,
	CONSTRAINT FK_Ca FOREIGN KEY(Microchip) REFERENCES CAVALLO(Microchip) ON DELETE CASCADE 
);



CREATE TABLE VISITAVETERINARIA(
	IDVisita Number PRIMARY KEY,
	Data Date,
	Referto VARCHAR(150),
	Terapia VARCHAR(150),
	Microchip char(5)  NOT NULL,
	CONSTRAINT FK_Cavallo FOREIGN KEY(Microchip) REFERENCES CAVALLO(Microchip)  ON DELETE CASCADE 
);

CREATE TABLE ENTRATA (
 	IDEntrata Number PRIMARY KEY,
 	Data Date,
 	Microchip char(5) NOT NULL,
	CONSTRAINT FK_Cava FOREIGN KEY(Microchip) REFERENCES CAVALLO (Microchip)  ON DELETE CASCADE );





